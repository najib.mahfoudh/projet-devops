import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Produit } from '../models/produit';

@Injectable({
  providedIn: 'root'
})
export class ProduitService {
 host="http://localhost:9090/apiproduct"
  constructor(private client:HttpClient) { }

public getallproducts():Observable<Produit[]>
{
return this.client.get<Produit[]>(this.host+"/all")
}
public detail(id:number):Observable<Produit>
{
  return this.client.get<Produit>(this.host+"/get/"+id)
}
}