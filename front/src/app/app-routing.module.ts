import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailComponent } from './detail/detail.component';
import { ListeProduitComponent } from './liste-produit/liste-produit.component';
import { NotfoundComponent } from './notfound/notfound.component';

const routes: Routes = [
  
  { path: 'liste', component: ListeProduitComponent },
  { path: 'liste/:id', component: DetailComponent },
  { path: '', redirectTo:"/liste",pathMatch:'full' },
  { path: '**', component: NotfoundComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
