import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Produit } from '../models/produit';
import { ProduitService } from '../services/produit.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  prd!:Produit
  constructor(private ar:ActivatedRoute,private service:ProduitService) { }

  ngOnInit(): void {
    let id=this.ar.snapshot.params['id']
    console.log(id)
    this.service.detail(id).subscribe(data=>this.prd=data)
  }

}
