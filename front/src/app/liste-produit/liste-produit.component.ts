import { Component, OnInit } from '@angular/core';
import { Produit } from '../models/produit';
import { ProduitService } from '../services/produit.service';

@Component({
  selector: 'app-liste-produit',
  templateUrl: './liste-produit.component.html',
  styleUrls: ['./liste-produit.component.css']
})
export class ListeProduitComponent implements OnInit {
  Produits!:Produit[];
  filterTerm!: string;
  constructor(private service:ProduitService) { }

  ngOnInit(): void {
    this.service.getallproducts().subscribe(data=>this.Produits=data)
  }
 /* public getrecherche(filterTerm: String){
    console.log(filterTerm)
    this.service.recherche(filterTerm).subscribe(data=>this.Produits=data)
  }*/
}
